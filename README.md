# Overview

This repository is set of proofs of concepts for how to write Python extensions using C/C++. We are using these wrappers: Cython, CPython and there is also mentioned SWIG. We have rewritten Python scripts using these wrappers for 3 steganalytical attack methods: 
* WS - Weighted stego-image analysis
* RS - Regular and Singular groups
* Chi - Chi-square Attack
Each of these methods have its own directory, where we place developed Cython/CPython code.

We found out that reading data from files is very slow using Numpy library, so we created our own reading function (just reads data to 2D array). Implementation is place in folder named IO.

We used library distutils to release libraries, to build some of the modules use this command:
```python setup.py build```

## Project Structure
----------

* Chi 
	* Cython version - implementation
		*speedtestchi - test file	
	* Python version - implementation, test file       
* IO
	* CPython - implementation
		* test - test file
* RS 
	* Cython - implementation
		* test - test file	  
* WS 
	* CPython 
		* v1 - implementation (original python, just rewritten)
			* test - test file
		* v2 - implementation - updated python ( new structure for tests )
			* test - test file
	* Cython
		* v1 - implementation (original python, just rewritten)
			* test - test file
		* v2 - implementation - updated python ( new structure for tests )
			* test - test file
* old 
	* SWIG 
	* C++  - old c++ code
		  
Note: Folder old can be deleted. We are not using it anymore.