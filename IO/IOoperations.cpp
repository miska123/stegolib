#include <Python.h>
#include <numpy/arrayobject.h>
#include <stdio.h>
#include <iostream>
#include "utils.cpp"

using namespace std;

/*
Function to load data into 2D vector without knowing dimensions
*/
std::vector< std::vector<int> > loadtxt(istream &file) {
	std::string str;
	std::vector <std::vector<int> > arr2D;
	
	while (std::getline(file, str))
	{
		std::stringstream iss(str);
		int number;
		std::vector<int> myNumbers;
		while (iss >> number)
			myNumbers.push_back(number);
		if (myNumbers.size() > 0) { //odignorujeme prazdne riadky
			arr2D.push_back(myNumbers);
		}

	}

	return arr2D;
}

/*
Function to load data into 2D vector knowing dimensions
*/
std::vector< std::vector<int> > loadtxt2(istream &file, int rows, int cols) {
	std::string str;
	std::vector <std::vector<int> > arr2D(rows, vector<int>(cols));
	int i = 0, j = 0;

	while (std::getline(file, str))
	{
		j = 0;
		if (!str.empty()) {
			std::stringstream iss(str);
			int number;
			while (iss >> number) {
				arr2D[i][j] = number;
				j++;
			}
			//next row
			i++;
		}
	}

	return arr2D;
}

/*
Function to load data into PyArrayObject (2D-array) knowing dimensions
*/
PyObject* loadtxt3(istream &file, int rows, int cols, int type_num = PyArray_INT) {
	PyArrayObject *matout;
	npy_intp dims[2] = { rows, cols };

	matout = (PyArrayObject*)PyArray_EMPTY(2, dims, type_num, 0);
	if (!matout) throw logic_error("Unable to allocate memory for Python array");
	
	npy_intp i = 0, j = 0;
	std::string str;
	
	while (std::getline(file, str)){
		j = 0;
		if (!str.empty()) {
			std::stringstream iss(str);
			int number;
			while (iss >> number) {
				*(int*)PyArray_GETPTR2(matout, i, j) = number;
				j++;
			}
			//next row
			i++;
		}
	}
	return PyArray_Return(matout);
}

template<typename T>
PyObject* vectorToMatrix(const vector<vector<T>> &data, int type_num= PyArray_INT) {
	// rows not empty
	/*if (!data.empty()) {

		// column not empty
		if (!data[0].empty()) {*/
			PyArrayObject *matout;
			npy_intp nRows = data.size();
			npy_intp nCols = data[0].size();
			npy_intp dims[2] = { nRows, nCols };
			
			matout = (PyArrayObject*)PyArray_EMPTY(2, dims, type_num, 0);
			if (!matout) throw logic_error("Unable to allocate memory for Python array");
			
			T *pointer = (T*)PyArray_DATA(matout);

			// copy vector line by line
			for (npy_intp i = 0; i < nRows; ++i) {

				if (data[i].size() != nCols) {
					throw(string("Can not convert vector<vector<T>> to np.array."));
				}

				copy(data[i].begin(), data[i].end(), pointer + i * nCols);
			}

			return PyArray_Return(matout);
/*
		}

	}*/
}


vector< vector<int> > path_to_matrix(char* path)
{
	ostringstream str;
	str.str(path);
	ifstream inputfile1(path);
	vector < vector<int> > matrix = loadtxt(inputfile1);

	return matrix;
}

PyObject* getMatrix(char* path) {
	vector< vector<int> > data = path_to_matrix(path);
	PyObject* rows = vectorToMatrix(data);
	return rows;
}

PyObject* getMatrix2(char* path, int rows, int cols) {
	ifstream inputfile1(path);
	vector < vector<int> > matrix = loadtxt2(inputfile1, rows, cols);
	PyObject* data = vectorToMatrix(matrix);
	return data;
}

PyObject* getMatrix3(char* path, int rows, int cols) {
	ifstream inputfile1(path);
	PyObject* data = loadtxt3(inputfile1, rows, cols);
	return data;
}
