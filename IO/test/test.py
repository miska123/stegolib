import timeit

#Setup using CPython with dynamic 2D-vector array
setup = """
import stegolib as ws

path_orig = "C:/timak/WSanalysis/inputDataWS/450sekvencne0-100%/000.gx0.1-2-4.orig.txt"
path_estim = "C:/timak/WSanalysis/inputDataWS/450sekvencne0-100%/000.gx0.1-2-4.t0.txt"

def load(path_orig, path_estim):
	orig = ws.ws_path_to_matrix( path_orig )
	estim = ws.ws_path_to_matrix( path_estim )
"""

#Setup using CPython with static 2D-vector array
setup2 = """
import stegolib as ws

path_orig = "C:/timak/WSanalysis/inputDataWS/450sekvencne0-100%/000.gx0.1-2-4.orig.txt"
path_estim = "C:/timak/WSanalysis/inputDataWS/450sekvencne0-100%/000.gx0.1-2-4.t0.txt"

def load(path_orig, path_estim, rows , cols):
	orig = ws.ws_path_to_matrix2( path_orig, rows, cols)
	estim = ws.ws_path_to_matrix2( path_estim, rows, cols)
"""

#Setup using CPython with static 2D-PyObjectArray array
setup3 = """
import stegolib as ws

path_orig = "C:/timak/WSanalysis/inputDataWS/450sekvencne0-100%/000.gx0.1-2-4.orig.txt"
path_estim = "C:/timak/WSanalysis/inputDataWS/450sekvencne0-100%/000.gx0.1-2-4.t0.txt"

def load(path_orig, path_estim, rows, cols ):
	orig = ws.ws_path_to_matrix3( path_orig , rows, cols )
	estim = ws.ws_path_to_matrix3( path_estim , rows, cols)
"""

#Setup using Cython
setup4 = """
import WS as ws
path_orig = "C:/timak/WSanalysis/inputDataWS/450sekvencne0-100%/000.gx0.1-2-4.orig.txt"
path_estim = "C:/timak/WSanalysis/inputDataWS/450sekvencne0-100%/000.gx0.1-2-4.t0.txt"
	
def test(path_orig, path_estim):
	orig = ws.path_to_matrix(path_orig)
	estimated = ws.path_to_matrix(path_estim)
"""

#Setup using Python code
setup5 = """
import WS_orig as ws
path_orig = "C:/timak/WSanalysis/inputDataWS/450sekvencne0-100%/000.gx0.1-2-4.orig.txt"
path_estim = "C:/timak/WSanalysis/inputDataWS/450sekvencne0-100%/000.gx0.1-2-4.t0.txt"

def test(path_orig, path_estim):
	orig = ws.path_to_matrix(path_orig)
	estimated = ws.path_to_matrix(path_estim)
"""

number = 100
def time(time):
	return str(time).replace('.',',')

timeCPy = timeit.timeit("load(path_orig, path_estim)", setup = setup, number = number)	
print("CPython dynamic vector:",time(timeCPy))

timeCPy2 = timeit.timeit("load(path_orig, path_estim, 1350, 256)", setup = setup2, number = number)	
print("CPython static vector:",time(timeCPy2))

timeCPy3 = timeit.timeit("load(path_orig, path_estim, 1350, 256)", setup = setup3, number = number)	
print("CPython PyObject:",time(timeCPy3))

timeCy = timeit.timeit("test(path_orig, path_estim)", setup = setup4, number = number)	
print("Cython: ", time(timeCy))

timeNoC = timeit.timeit("test(path_orig, path_estim)", setup = setup5, number = number)	
print("Python: ", time(timeNoC))