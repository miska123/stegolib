#include <Python.h>
#include <numpy/arrayobject.h>
#include <stdio.h>
#include <iostream>
#include <vector>
#include <fstream>
#include <sstream>
#include <string>
#include <iomanip>
#include "iooperations.cpp"

using namespace std;

PyObject* load(PyObject *self, PyObject *arg) {
	
	char *path;

	if (!PyArg_Parse(arg, "s", &path))
		return NULL;
	
	PyObject* matrix = getMatrix( path );
	return matrix;
}


PyObject* load2(PyObject *self, PyObject *arg) {

	char *path;
	int rows, cols;

	if (!PyArg_ParseTuple(arg, "sii", &path, &rows, &cols))
		return NULL;

	PyObject* matrix = getMatrix2(path, rows, cols);

	return matrix;
}



PyObject* load3(PyObject *self, PyObject *arg) {

	char *path;
	int rows, cols;

	if (!PyArg_ParseTuple(arg, "sii", &path, &rows, &cols))
		return NULL;

	PyObject* matrix = getMatrix3(path, rows, cols);

	return matrix;
}

static PyMethodDef stego_methods[] = {
	// The first property is the name exposed to python,
	// the second is the C++ function name        
	{ "ws_path_to_matrix", (PyCFunction)load, METH_O, "Load data from file" },
	{ "ws_path_to_matrix2", (PyCFunction)load2, METH_VARARGS, "Load data from file" },
	{ "ws_path_to_matrix3", (PyCFunction)load3, METH_VARARGS, "Load data from file" },

	// Terminate the array with an object containing nulls.
	{ nullptr, nullptr, 0, nullptr }
};

static PyModuleDef stegolib_module = {
	PyModuleDef_HEAD_INIT,
	"Stegolib",                        // Module name as Python sees it
	"Stego analalysis library",  // Module description
	0,
	stego_methods						  // Structure that defines the methods
};

PyMODINIT_FUNC PyInit_stegolib() {
	import_array();
	return PyModule_Create(&stegolib_module);
}