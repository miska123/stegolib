from distutils.core import setup, Extension
import numpy as np

sfc_module = Extension('stegolib', sources = ['module.cpp'])

setup(name = 'stegolib', version = '1.4',
    description = 'CPython Package for WS analysis C++ Extension',
	include_dirs = [np.get_include()],
    ext_modules = [sfc_module]
)