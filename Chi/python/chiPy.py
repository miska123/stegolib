import numpy as np
from scipy.stats.mstats import chisquare
#from scipy.stats import chisquare


def FileLoad(filename):
    histogram_matrix = np.loadtxt(filename, delimiter=',')
    #histogram_matrix = np.loadtxt(filename)
    return histogram_matrix

def chiPy(histogram_matrix):
    num_of_rows = histogram_matrix.shape[0]
    num_of_cols = histogram_matrix.shape[1]

    h = np.zeros(num_of_rows)
    p = np.zeros(num_of_rows)
    expected = np.zeros(num_of_cols//2)

    for k in range(0, num_of_rows):
        even_data = histogram_matrix[k, 0::2]
        odd_data = histogram_matrix[k, 1::2]

        num_of_stats = np.size(even_data)
        # compute expected frequencies
        for n in range(0, num_of_stats):
            expected[n] = ((even_data[n] + odd_data[n]) / 2)
        # delete frequency of DCT coefficient with value 0
        # the most frequent one
        even_data[num_of_stats // 2] = 1
        expected[num_of_stats // 2] = 1
        # !!!
        (h[k], p[k]) = chisquare(even_data, expected)
    return (h,p)    

def FileSave(path,data):
    np.savetxt(path+'ChiStegoResult.txt', data)
    

# for testing purposes
if __name__ == '__main__':
    # ignore warnings
    np.seterr(divide='ignore', invalid='ignore')
    chiPy()