import timeit

setup ="""
import chiPy as cpc
path = "chi_stego.txt"

histogram_matrix = cpc.FileLoad(path)
"""

setup2 ="""
import chiPy_orig as cps
path = "chi_stego.txt"

histogram_matrix = cps.FileLoad(path)
"""

number = 1000

timeCy = timeit.timeit("cpc.chiPy(histogram_matrix)", setup = setup, number = number)	
print("Cython: ",timeCy)

timeNoC = timeit.timeit("cps.chiPy(histogram_matrix)", setup = setup2, number = number)	
print("Python: ", timeNoC)

	
	