import numpy as np
from scipy.stats.mstats import chisquare
#from scipy.stats import chisquare

import cython
cimport cython
cimport numpy as np

DTYPE = np.int
ctypedef np.int_t DTYPE_t

cpdef np.ndarray[DTYPE_t, ndim=2] FileLoad(filename):
    cdef np.ndarray[DTYPE_t, ndim=2] histogram_matrix = np.loadtxt(filename, delimiter=',', dtype=DTYPE)
    #histogram_matrix = np.loadtxt(filename)
    return histogram_matrix

cpdef chiPy(np.ndarray[DTYPE_t, ndim=2]histogram_matrix):
    cdef unsigned int num_of_rows = histogram_matrix.shape[0]
    cdef unsigned int num_of_cols = histogram_matrix.shape[1]

    cdef np.ndarray[DTYPE_t, ndim=1] h = np.zeros(num_of_rows,dtype=DTYPE)
    cdef np.ndarray[DTYPE_t, ndim=1] p = np.zeros(num_of_rows,dtype=DTYPE)
    cdef np.ndarray[DTYPE_t, ndim=1] expected = np.zeros(num_of_cols//2,dtype=DTYPE)
    
    cdef unsigned int k
    cdef unsigned int n
    
    cdef np.ndarray[DTYPE_t, ndim=1] even_data
    cdef np.ndarray[DTYPE_t, ndim=1] odd_data
    cdef unsigned int num_of_stats
    
    for k in range(0, num_of_rows):
        even_data = histogram_matrix[k, 0::2]
        odd_data = histogram_matrix[k, 1::2]

        num_of_stats = np.size(even_data)
        # compute expected frequencies
        for n in range(0, num_of_stats):
            expected[n] = ((even_data[n] + odd_data[n]) / 2)
        # delete frequency of DCT coefficient with value 0
        # the most frequent one
        even_data[num_of_stats // 2] = 1
        expected[num_of_stats // 2] = 1
        # !!!
        (h[k], p[k]) = chisquare(even_data, expected)
    return (h,p)    

def FileSave(path,data):
    np.savetxt(path+'ChiStegoResult.txt', data)