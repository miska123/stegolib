from distutils.core import setup, Extension
from Cython.Build import cythonize
import numpy

setup(
  name = 'Chi kvadrat test',
  ext_modules = cythonize(Extension(
  'chiPy',
   sources=['chiPy.pyx'],
   language='c',
   include_dirs=[numpy.get_include()]))
)