# -*- coding: utf-8 -*-
"""
Created on Sun Apr 24 10:37:02 2016

@author: Michala
"""

import cython
cimport cython
import numpy as np
cimport numpy as np

DTYPE = np.int
ctypedef np.int_t DTYPE_t

cpdef np.ndarray[DTYPE_t, ndim=2] DCT
	
cpdef path_to_dct(str path):
	DCT = np.genfromtxt(path, skip_header = 1)
	
#len pre 1 frekvenciu
cpdef vyber1frekvenciu(unsigned int index_row, unsigned int index_col, unsigned int y):
	cdef unsigned int num_rows = int(len(DCT)/8)
	cdef unsigned int num_cols = int((DCT.size/len(DCT))/8)
	cdef np.ndarray[DTYPE_t, ndim=2] G = empty([num_rows, num_cols])
	
	cdef unsigned int row_G = 0
	cdef unsigned int jump = 0
	for y in range(index_row, len(DCT), 8):
		#print("y:", y)
		#pre kazdy nulty riadok - vynechavame AC koeficienty
		jump = index_col
		for x in range(0, int((DCT.size/len(DCT))/8)):
			G[row_G][x]   = DCT[y][jump]
			jump += 8
		row_G+= 1
		#print("row_G:", row_G)
	return G
	
cpdef odstran01(np.ndarray[DTYPE_t, ndim=2] G):
   
	#G = G[logical_not(equal(G, 0) | equal(G, 1))]
	G = G[logical_not(equal(G, 0) | equal(G, 1))]
	return G

	#vytvorime si vlastnu diskriminacnu funkciu
cpdef myVariation(np.ndarray[DTYPE_t, ndim=1] vector):
	cdef unsigned int variation = 0
	for i in range(0, len(vector)-1):
		variation += abs(vector[i+1] - vector[i])
	return variation
	
cpdef velkostSkupin(unsigned int size):
	if size%5 == 0:
		return 5
	elif size%4 == 0:
		return 4
	elif size%3 == 0:
		return 3
	elif size%6 == 0:
		return 6
	else:
		print("Nie je delitelna 6, 5, 4, ani 3!!!")
		return 0

cpdef odrez(np.ndarray[DTYPE_t, ndim=2] G):
	n = G.size % 4 # upravi na velkost delit. 5
	print("orezavam:", n, "prvkov")
	G = G[:(G.size-n)]
	return G
	
cpdef discriminacnaF(np.ndarray[DTYPE_t, ndim=2] G):
	cdef unsigned int f_G = [0] * len(G) #pole f(G)    
	
	for i in range(0, len(G)):
		f_G[i] = myVariation(G[i]) #numpy var - population variance
		#print("var G[",i,"]:", var(G[i]))
		#print("myVar G[",i,"]:", myVariation(G[i]))   
	return f_G
	
cpdef createMask(unsigned int value, np.ndarray[DTYPE_t, ndim=2] G):
	cdef np.ndarray[DTYPE_t, ndim=2] M = zeros((1, len(G[0])))
	for m in range(len(G[0])-(len(G[0])-1), len(G[0]) - 1):
		M[0][m] = value
	return M
	
cpdef flipping1(unsigned int number): # permutacie
	if number%2 == 0: # ak je parne
		number += 1
		return number
	else:
		number -= 1
		return number
		
cpdef flipping2(unsigned int number): # inverzny flipping operation
	if number%2 != 0: # ak nie je parne
		number += 1
		return number
	else:
		number -= 1
		return number
		
cpdef applyMask(np.ndarray[DTYPE_t, ndim=2] maskedG, np.ndarray[DTYPE_t, ndim=2] M):
	for rows in range(0, len(maskedG)):
		for cols in range(0, len(maskedG[0])):
			if M[0][cols] == 1:
				maskedG[rows][cols] = flipping1(maskedG[rows][cols])
			elif M[0][cols] == -1:
				maskedG[rows][cols] = flipping2(maskedG[rows][cols])
			else:
				maskedG[rows][cols] = maskedG[rows][cols]
	return maskedG
	
cpdef weighted_image():

	cdef np.ndarray[DTYPE_t, ndim=2] regular = zeros(( 101 ))
	cdef np.ndarray[DTYPE_t, ndim=2] singular = zeros(( 101 ))
	cdef np.ndarray[DTYPE_t, ndim=2] regular_ = zeros(( 101 ))
	cdef np.ndarray[DTYPE_t, ndim=2] singular_ = zeros(( 101 ))
	cdef np.ndarray[DTYPE_t, ndim=2] rozdielR = zeros(( 101 ))
	cdef np.ndarray[DTYPE_t, ndim=2] rozdielS = zeros(( 101 ))
	cdef np.ndarray[DTYPE_t, ndim=2] rozdielRS = zeros(( 101 ))
	cdef unsigned int index_row = None
	cdef unsigned int index_col = None
	cdef np.ndarray[DTYPE_t, ndim=2] G = None
	cdef unsigned int n = None
	cdef np.ndarray[DTYPE_t, ndim=1] f_G = None
	cdef np.ndarray[DTYPE_t, ndim=2] M = None
	cdef np.ndarray[DTYPE_t, ndim=2] maskedG = None
	cdef np.ndarray[DTYPE_t, ndim=1] f_maskedG = None
	cdef unsigned int Regular  = None
	cdef unsigned int Singular = None
	cdef unsigned int Unusable = None
	cdef np.ndarray[DTYPE_t, ndim=2] inv_M = None
	cdef np.ndarray[DTYPE_t, ndim=2] inv_maskedG = None
	cdef np.ndarray[DTYPE_t, ndim=2] f_inv_maskedG = None
	cdef unsigned int Regular_  = None
	cdef unsigned int Singular_ = None
	cdef unsigned int Unusable_ = None
	
	for y in range(0, 101):
		

		#    print("pocet row_G:", row_G)
		#    print("pocet riadkov G:", num_rows)        
		#    print(G[0])
			

			
		index_row = 0
		index_col = 1
		G = vyber1frekvenciu(index_row, index_col, y)
		print("G[0] pred:", G[0])
		print("size:", G.size)
		#G = odstran01(G)
		#print("G po:", G[0],G[1],G[2],G[3],G[4],G[5],G[6],G[7],G[8],G[9], "atd...")
		print("size:", G.size)
		#G = odrez(G)
		print("size:", G.size)
		
		n = velkostSkupin(G.size)
		if n == 0:
			G = odrez(G)
			n = velkostSkupin(G.size)
		print("velkost skupiniek je:", n)
		#este dalsie zmensenie skupin - skupinu o velkosti 250 rozdelit na skupiny po 5... 250/50 = 5
		G = G.reshape(-1, n)
		#print("reshapedG[0]:", G[0])
		 
		#3. aplikujeme diskriminacnu funkciu f(x_1, ..., x_N) = sum(abs(x_i - x_i-1)) ... variaciu... teda kazdej skupine G[i] priradime realne cislo
		
		f_G = discriminacnaF(G)
		#print(f_G)
		#print("dlzka vektora variacii f_G = ", len(f_G))
			
		#4. definujeme si masku M = (0..., 1, 1, ...1, 0..), ktora ma velkost dlzky vektora G[i], s tymto sa treba hrat!!!   
		#print(len(G[0])) 
			
		M = createMask(1, G)
		#print("maska M = ", M)
		
		#5. aplikujeme aditivnu masku M na skupiny matice G -> vznikne maskedG
				

		
		maskedG = copy(G) #maskedG = G[:] nefunguje, pretoze numpy vytvara objekty

			
		maskedG = applyMask(maskedG, M)
		#print("maskedG[0]:", maskedG[0])
		
		#6. aplikujeme diskriminacnu funkciu f na maskedG
		 
		f_maskedG = discriminacnaF(maskedG)
		
		#print("dlzka vektora variacii f_maskedG = ", len(f_maskedG))
		
		#7. porovname f_G a f_maskedG a priradime podla pravidla:
		# regular  <=> f_maskedG > f_G
		# singular <=> f_maskedG < f_G
		# unusable <=> f_maskedG = f_G
		
		Regular  = 0
		Singular = 0
		Unusable = 0
		for i in range(0, len(f_G)):
			if f_maskedG[i] > f_G[i]:
				Regular  += 1
				#print("regular ", i)
			elif f_maskedG[i] < f_G[i]:
				Singular += 1
				#print("singular ", i)
			else:
				Unusable += 1
		print("relat. pocet Regular skupin = ", Regular/len(f_G)*100, "%")
		print("relat. pocet Singular skupin = ", Singular/len(f_G)*100, "%")
		print("relat. pocet Unusable skupin = ", Unusable/len(f_G)*100, "%")
		
		#8. aplikujeme "inverznu masku M" **********************
		   # print("G before inverze", G[0])
		inv_M = createMask(-1, G)
		  #  print("inverzna maska M = ", inv_M)
		inv_maskedG = G # odovzdam referenciu na objekt
		inv_maskedG = applyMask(inv_maskedG, inv_M)
		   # print("inv_maskedG[0]:", inv_maskedG[0])
		
		#9. zopakujeme len s inverznym polovom
		f_inv_maskedG = discriminacnaF(inv_maskedG)
			
		Regular_  = 0
		Singular_ = 0
		Unusable_ = 0
		for i in range(0, len(f_G)):
			if f_inv_maskedG[i] > f_G[i]:
				Regular_ += 1
			elif f_inv_maskedG[i] < f_G[i]:
				Singular_ += 1
			else:
				Unusable_ += 1
		print("relat. pocet Regular  inverz. skupin = ", Regular_/len(f_G)*100, "%")
		print("relat. pocet Singular inverz. skupin = ", Singular_/len(f_G)*100, "%")
		print("relat. pocet Unusable inverz. skupin = ", Unusable_/len(f_G)*100, "%")
		
		regular[y] = Regular/len(f_G)*100
		singular[y]= Singular/len(f_G)*100
		regular_[y]= Regular_/len(f_G)*100
		singular_[y]=Singular_/len(f_G)*100
		
		rozdielR[y] = abs( (Regular/len(f_G)*100) - (Regular_/len(f_G)*100) )
		rozdielS[y] = abs( (Singular/len(f_G)*100) - (Singular_/len(f_G)*100) )
		rozdielRS[y] = abs( (Regular/len(f_G)*100) - (Singular/len(f_G)*100) )
	
	return regular, singular, regular_, singular_, rozdielR, rozdielS, rozdielRS
		
#savetxt('D:/skola/diplomova_praca/RSanalyza/txt/regularLSBperm.txt', regular)
#savetxt('D:/skola/diplomova_praca/RSanalyza/txt/singularLSBperm.txt', singular)
#savetxt('D:/skola/diplomova_praca/RSanalyza/txt/regular_LSBperm.txt', regular_)
#savetxt('D:/skola/diplomova_praca/RSanalyza/txt/singular_LSBperm.txt', singular_)
#savetxt('D:/skola/diplomova_praca/RSanalyza/txt/rozdielRLSBperm.txt', rozdielR)
#savetxt('D:/skola/diplomova_praca/RSanalyza/txt/rozdielSLSBperm.txt', rozdielS)
#savetxt('D:/skola/diplomova_praca/RSanalyza/txt/rozdielRSLSBperm.txt', rozdielRS)
         
