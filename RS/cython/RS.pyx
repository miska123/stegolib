# -*- coding: utf-8 -*-
"""
Created on Sun Apr 24 10:37:02 2016

@author: Michala
"""


#Mali by sme importovat vsetko? --MB
from pylab import * # importuje vsetko z NumPy, MatPlotLib, SciPy...

import cython
cimport cython
import numpy as np
cimport numpy as np

DTYPE = np.int
ctypedef np.int_t DTYPE_t

cpdef vyber1frekvenciu(index_row, index_col,DCT):
	cdef unsigned int num_rows = int(len(DCT)/8)
	cdef unsigned int num_cols = int((DCT.size/len(DCT))/8)
	cdef np.ndarray[DTYPE_t, ndim=2] G = empty([num_rows, num_cols],dtype=DTYPE)
	
	cdef unsigned int row_G = 0
	cdef unsigned int x = 0
	cdef unsigned int y = 0
	cdef unsigned int jump = 0
	for y in range(index_row, len(DCT), 8):
		jump = index_col
		for x in range(0, int((DCT.size/len(DCT))/8)):
			G[row_G][x]   = DCT[y][jump]
			jump += 8
		row_G+= 1
	return G
	
cpdef odstran01(G):
   
	G = G[logical_not(equal(G, 0) | equal(G, 1))] # co je toto? --MB
	return G
	
cpdef myVariation(vector):
	cdef unsigned int variation = 0
	for i in range(0, len(vector)-1):
		variation += abs(vector[i+1] - vector[i])
	return variation
	
cpdef velkostSkupin(size):
	if size%5 == 0:
		return 5
	elif size%4 == 0:
		return 4
	elif size%3 == 0:
		return 3
	elif size%6 == 0:
		return 6
	else:
		print("Nie je delitelna 6, 5, 4, ani 3!!!")
		return 0
		
cpdef odrez(G):
	cdef unsigned int n = G.size % 4 # upravi na velkost delit. 5
	print("orezavam:", n, "prvkov")
	G = G[:(G.size-n)]
	return G
cpdef discriminacnaF(np.ndarray[DTYPE_t, ndim=2] G):
	cdef list f_G = [0] * len(G)
	cdef unsigned int i = 0
	for i in range(0, len(G)):
		f_G[i] = myVariation(G[i]) 
	return f_G
cpdef createMask(value,G):
	cdef np.ndarray[DTYPE_t, ndim=2] M = zeros((1, len(G[0])),dtype=DTYPE)
	cdef unsigned int m = 0
	for m in range(len(G[0])-(len(G[0])-1), len(G[0]) - 1):
		M[0][m] = value
	return M 
cpdef flipping1(number):
	if number%2 == 0:
		number += 1
		return number
	else:
		number -= 1
		return number
		
cpdef flipping2(number): 
	if number%2 != 0:
		number += 1
		return number
	else:
		number -= 1
		return number
cpdef applyMask(maskedG, M):
	cdef unsigned int rows = 0
	cdef unsigned int cols = 0
	for rows in range(0, len(maskedG)):
		for cols in range(0, len(maskedG[0])):
			if M[0][cols] == 1:
				maskedG[rows][cols] = flipping1(maskedG[rows][cols])
			elif M[0][cols] == -1:
				maskedG[rows][cols] = flipping2(maskedG[rows][cols])
			else:
				maskedG[rows][cols] = maskedG[rows][cols]
	return maskedG

cpdef np.ndarray[DTYPE_t, ndim=2] path_to_dct(path):
	cdef np.ndarray[DTYPE_t, ndim=2] DCT = np.genfromtxt(path, skip_header = 1,dtype=DTYPE)
	return DCT
	

cpdef weighted_image(DCT):
	cdef unsigned int regular
	cdef unsigned int singular
	cdef unsigned int regular_
	cdef unsigned int singular_
	cdef unsigned int rozdielR
	cdef unsigned int rozdielS
	cdef unsigned int rozdielRS
	cdef unsigned int y
	cdef unsigned int index_row
	cdef unsigned int index_col
	cdef np.ndarray[DTYPE_t, ndim=2] G
	cdef unsigned int n
	cdef list f_G
	cdef list f_maskedG
	cdef np.ndarray[DTYPE_t, ndim=2] M
	cdef np.ndarray[DTYPE_t, ndim=2] maskedG
	cdef unsigned int Regular_
	cdef unsigned int Singular_
	cdef unsigned int Unusable_
	cdef unsigned int Regular
	cdef unsigned int Singular
	cdef unsigned int Unusable
	cdef unsigned int i
	cdef np.ndarray[DTYPE_t, ndim=2] inv_M
	cdef np.ndarray[DTYPE_t, ndim=2] inv_maskedG
	cdef list f_inv_maskedG 
	
	
	index_row = 0
	index_col = 1
	G = vyber1frekvenciu(index_row, index_col,DCT)
	n = velkostSkupin(G.size)
	if n == 0:
		G = odrez(G)
		n = velkostSkupin(G.size)
	G = G.reshape(-1, n)
	
	f_G = discriminacnaF(G)
	M = createMask(1,G)
	maskedG = copy(G)		
	maskedG = applyMask(maskedG, M)
	f_maskedG = discriminacnaF(maskedG)
	
	Regular  = 0
	Singular = 0
	Unusable = 0
	i = 0
	for i in range(0, len(f_G)):
		if f_maskedG[i] > f_G[i]:
			Regular  += 1
		elif f_maskedG[i] < f_G[i]:
			Singular += 1
		else:
			Unusable += 1
	inv_M = createMask(-1,G)
	inv_maskedG = G
	inv_maskedG = applyMask(inv_maskedG, inv_M)
	f_inv_maskedG = discriminacnaF(inv_maskedG)
		
	Regular_  = 0
	Singular_ = 0
	Unusable_ = 0
	for i in range(0, len(f_G)):
		if f_inv_maskedG[i] > f_G[i]:
			Regular_ += 1
		elif f_inv_maskedG[i] < f_G[i]:
			Singular_ += 1
		else:
			Unusable_ += 1
	
	regular = Regular/len(f_G)*100
	singular = Singular/len(f_G)*100
	regular_= Regular_/len(f_G)*100
	singular_=Singular_/len(f_G)*100
	
	rozdielR = abs( (Regular/len(f_G)*100) - (Regular_/len(f_G)*100) )
	rozdielS = abs( (Singular/len(f_G)*100) - (Singular_/len(f_G)*100) )
	rozdielRS = abs( (Regular/len(f_G)*100) - (Singular/len(f_G)*100) )

	return regular, singular, regular_, singular_, rozdielR, rozdielS, rozdielRS
         
