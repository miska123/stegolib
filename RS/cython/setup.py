from distutils.core import setup, Extension
from Cython.Build import cythonize
import numpy

setup(
  name = 'RS test',
  ext_modules = cythonize(Extension(
  'RS',
   sources=['RS.pyx'],
   language='c',
   include_dirs=[numpy.get_include()]))
)