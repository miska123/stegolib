# -*- coding: utf-8 -*-
"""
Created on Sun Apr 24 10:37:02 2016

@author: Michala
"""

from pylab import * # importuje vsetko z NumPy, MatPlotLib, SciPy...

regular = zeros(( 101 ))
singular = zeros(( 101 ))
regular_ = zeros(( 101 ))
singular_ = zeros(( 101 ))
rozdielR = zeros(( 101 ))
rozdielS = zeros(( 101 ))
rozdielRS = zeros(( 101 ))

    # 1. nacitaj DCT koeficienty zo suboru
def path_to_dct(path):
	DCT = genfromtxt(path, skip_header = 1)
	return DCT
    
    #len pre 1 frekvenciu
def vyber1frekvenciu(index_row, index_col,DCT):
	num_rows = int(len(DCT)/8)
	num_cols = int((DCT.size/len(DCT))/8)
	G = empty([num_rows, num_cols])
	
	row_G = 0
	for y in range(index_row, len(DCT), 8):
		#print("y:", y)
		#pre kazdy nulty riadok - vynechavame AC koeficienty
		jump = index_col
		for x in range(0, int((DCT.size/len(DCT))/8)):
			G[row_G][x]   = DCT[y][jump]
			jump += 8
		row_G+= 1
		#print("row_G:", row_G)
	return G
#    print("pocet row_G:", row_G)
#    print("pocet riadkov G:", num_rows)        
#    print(G[0])
	
def odstran01(G):
   
	G = G[logical_not(equal(G, 0) | equal(G, 1))]
	return G
	
	#vytvorime si vlastnu diskriminacnu funkciu
def myVariation(vector):
	variation = 0
	for i in range(0, len(vector)-1):
		variation += abs(vector[i+1] - vector[i])
	return variation
	
def velkostSkupin(size):
	if size%5 == 0:
		return 5
	elif size%4 == 0:
		return 4
	elif size%3 == 0:
		return 3
	elif size%6 == 0:
		return 6
	else:
		print("Nie je delitelna 6, 5, 4, ani 3!!!")
		return 0
		
def odrez(G):
	n = G.size % 4 # upravi na velkost delit. 5
	print("orezavam:", n, "prvkov")
	G = G[:(G.size-n)]
	return G

def weighted_image(DCT):		
    index_row = 0
    index_col = 1
    G = vyber1frekvenciu(index_row, index_col,DCT)
    #G = odstran01(G)
    #print("G po:", G[0],G[1],G[2],G[3],G[4],G[5],G[6],G[7],G[8],G[9], "atd...")
    #G = odrez(G)
    n = velkostSkupin(G.size)
    if n == 0:
        G = odrez(G)
        n = velkostSkupin(G.size)
    #este dalsie zmensenie skupin - skupinu o velkosti 250 rozdelit na skupiny po 5... 250/50 = 5
    G = G.reshape(-1, n)
    #print("reshapedG[0]:", G[0])
     
    #3. aplikujeme diskriminacnu funkciu f(x_1, ..., x_N) = sum(abs(x_i - x_i-1)) ... variaciu... teda kazdej skupine G[i] priradime realne cislo
    def discriminacnaF(G):
        f_G = [0] * len(G) #pole f(G)    
        
        for i in range(0, len(G)):
            f_G[i] = myVariation(G[i]) #numpy var - population variance
            #print("var G[",i,"]:", var(G[i]))
            #print("myVar G[",i,"]:", myVariation(G[i]))   
        return f_G
    
    f_G = discriminacnaF(G)
    #print(f_G)
    #print("dlzka vektora variacii f_G = ", len(f_G))
        
    #4. definujeme si masku M = (0..., 1, 1, ...1, 0..), ktora ma velkost dlzky vektora G[i], s tymto sa treba hrat!!!   
    #print(len(G[0]))
    def createMask(value):
        M = zeros((1, len(G[0])))
        for m in range(len(G[0])-(len(G[0])-1), len(G[0]) - 1):
            M[0][m] = value
        return M  
        
    M = createMask(1)
    #print("maska M = ", M)
    
    #5. aplikujeme aditivnu masku M na skupiny matice G -> vznikne maskedG
    def flipping1(number): # permutacie
        if number%2 == 0: # ak je parne
            number += 1
            return number
        else:
            number -= 1
            return number
            
    def flipping2(number): # inverzny flipping operation
        if number%2 != 0: # ak nie je parne
            number += 1
            return number
        else:
            number -= 1
            return number
    
    maskedG = copy(G) #maskedG = G[:] nefunguje, pretoze numpy vytvara objekty
    
    def applyMask(maskedG, M):
        for rows in range(0, len(maskedG)):
            for cols in range(0, len(maskedG[0])):
                if M[0][cols] == 1:
                    maskedG[rows][cols] = flipping1(maskedG[rows][cols])
                elif M[0][cols] == -1:
                    maskedG[rows][cols] = flipping2(maskedG[rows][cols])
                else:
                    maskedG[rows][cols] = maskedG[rows][cols]
        return maskedG
        
    maskedG = applyMask(maskedG, M)
    #print("maskedG[0]:", maskedG[0])
    
    #6. aplikujeme diskriminacnu funkciu f na maskedG
     
    f_maskedG = discriminacnaF(maskedG)
    
    #print("dlzka vektora variacii f_maskedG = ", len(f_maskedG))
    
    #7. porovname f_G a f_maskedG a priradime podla pravidla:
    # regular  <=> f_maskedG > f_G
    # singular <=> f_maskedG < f_G
    # unusable <=> f_maskedG = f_G
    
    Regular  = 0
    Singular = 0
    Unusable = 0
    for i in range(0, len(f_G)):
        if f_maskedG[i] > f_G[i]:
            Regular  += 1
            #print("regular ", i)
        elif f_maskedG[i] < f_G[i]:
            Singular += 1
            #print("singular ", i)
        else:
            Unusable += 1
    
    #8. aplikujeme "inverznu masku M" **********************
       # print("G before inverze", G[0])
    inv_M = createMask(-1)
      #  print("inverzna maska M = ", inv_M)
    inv_maskedG = G # odovzdam referenciu na objekt
    inv_maskedG = applyMask(inv_maskedG, inv_M)
       # print("inv_maskedG[0]:", inv_maskedG[0])
    
    #9. zopakujeme len s inverznym polovom
    f_inv_maskedG = discriminacnaF(inv_maskedG)
        
    Regular_  = 0
    Singular_ = 0
    Unusable_ = 0
    for i in range(0, len(f_G)):
        if f_inv_maskedG[i] > f_G[i]:
            Regular_ += 1
        elif f_inv_maskedG[i] < f_G[i]:
            Singular_ += 1
        else:
            Unusable_ += 1
    
    regular = Regular/len(f_G)*100
    singular = Singular/len(f_G)*100
    regular_ = Regular_/len(f_G)*100
    singular_ =Singular_/len(f_G)*100
    
    rozdielR  = abs( (Regular/len(f_G)*100) - (Regular_/len(f_G)*100) )
    rozdielS  = abs( (Singular/len(f_G)*100) - (Singular_/len(f_G)*100) )
    rozdielRS  = abs( (Regular/len(f_G)*100) - (Singular/len(f_G)*100) )
    
         

