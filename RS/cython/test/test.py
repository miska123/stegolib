import timeit

setup ="""
import RS as rss
path = "stego_out_lsb.txt"

DCT = rss.path_to_dct(path)
"""

setup2 ="""
import RS_orig as rso
path = "stego_out_lsb.txt"

DCT = rso.path_to_dct(path)
"""

number = 1000

timeCy = timeit.timeit("rss.weighted_image(DCT)", setup = setup, number = number)	
print("Cython: ",timeCy)

timeNoC = timeit.timeit("rso.weighted_image(DCT)", setup = setup2, number = number)	
print("Python: ", timeNoC)

	
	