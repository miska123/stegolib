# -*- coding: utf-8 -*-
#define NPY_NO_DEPRECATED_API NPY_1_7_API_VERSION
"""
Created on Fri Nov 20 17:16:14 2015

@author: Michala
"""
import cython
import numpy as np
cimport numpy as cnp

@cython.boundscheck(False)
@cython.wraparound(False)
@cython.cdivision(True)
@cython.nonecheck(False)
cdef cycle(int y, str path1, str path2):
	cdef str path_orig = path1%y
	cdef str path_t0 = path2%y
	#cdef cnp.float32_t[:,:] orig = np.loadtxt(path_orig, dtype=np.float32)
	#cdef cnp.float32_t[:,:] estim = np.loadtxt(path_t0, dtype=np.float32)
	cdef double[:,:] orig = np.loadtxt(path_orig)
	cdef double[:,:] estim = np.loadtxt(path_t0)
	cdef size_t size = len(orig)/3
	
	cdef int a = 3
	cdef cnp.float32_t[:,:] BETAkl = np.empty(( size , a ), dtype=np.float32)
	cdef cnp.ndarray[dtype=cnp.float32_t,ndim=2] BETA = np.zeros(( size ,1), dtype=np.float32)
	
	cdef int row = 0
	cdef size_t length = len(orig)
	cdef int i
	for i in range(0, length , 3):
		BETAkl[row,0]= ((estim[i,126]-estim[i,127])*(estim[i,126]-estim[i,127])-(estim[i,126]-estim[i,127])*(orig[i,126]-orig[i,127])+(estim[i,131]-estim[i,130])*(estim[i,131]-estim[i,130])-(estim[i,131]-estim[i,130])*(orig[i,131]-orig[i,130]))/(2*(estim[i,126]-estim[i,127])*(estim[i,126]-estim[i,127])+2*(estim[i,131]-estim[i,130])*(estim[i,131]-estim[i,130]));
		BETAkl[row,1] = ((estim[i+1,126]-estim[i+1,127])*(estim[i+1,126]-estim[i+1,127])-(estim[i+1,126]-estim[i+1,127])*(orig[i+1,126]-orig[i+1,127])+(estim[i+1,131]-estim[i+1,130])*(estim[i+1,131]-estim[i+1,130])-(estim[i+1,131]-estim[i+1,130])*(orig[i+1,131]-orig[i+1,130]))/(2*(estim[i+1,126]-estim[i+1,127])*(estim[i+1,126]-estim[i+1,127])+2*(estim[i+1,131]-estim[i+1,130])*(estim[i+1,131]-estim[i+1,130]));
		BETAkl[row,2] = ((estim[i+2,126]-estim[i+2,127])*(estim[i+2,126]-estim[i+2,127])-(estim[i+2,126]-estim[i+2,127])*(orig[i+2,126]-orig[i+2,127])+(estim[i+2,131]-estim[i+2,130])*(estim[i+2,131]-estim[i+2,130])-(estim[i+2,131]-estim[i+2,130])*(orig[i+2,131]-orig[i+2,130]))/(2*(estim[i+2,126]-estim[i+2,127])*(estim[i+2,126]-estim[i+2,127])+2*(estim[i+2,131]-estim[i+2,130])*(estim[i+2,131]-estim[i+2,130]));
		BETA[row,0] = (BETAkl[row,0]+BETAkl[row,1]+BETAkl[row,2])/3;    
		row +=1;

def ws():
	cdef str path_orig = "C:/timak/WSanalysis/inputDataWS/450sekvencne0-100%%/%03d.gx0.1-2-4.orig.txt"
	cdef str path_t0 = "C:/timak/WSanalysis/inputDataWS/450sekvencne0-100%%/%03d.gx0.1-2-4.t0.txt"
	cdef int y
	
	for y in range(0, 101):
		cycle(y, path_orig, path_t0)
