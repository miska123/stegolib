# -*- coding: utf-8 -*-
"""
Created on Fri Nov 20 17:16:14 2015

@author: Michala
"""

import numpy as np

#vypocet bety pre vsetky obrazky - prevzaty matlab subor betaALL.m
# mapovanie: -2 = 126, -1 = 127, 2 = 130, 3 = 131

def path_to_matrix(path):
    matrix = np.loadtxt(path)
    return matrix

def weighted_image( orig, estim):
    size = int( len(orig)/3)
    
    BETAkl = np.zeros(( size , 3 ));
    
    row = 0
    for i in range(0, len(orig), 3):
        BETAkl[row,0]= ((estim[i,126]-estim[i,127])*(estim[i,126]-estim[i,127])-(estim[i,126]-estim[i,127])*(orig[i,126]-orig[i,127])+(estim[i,131]-estim[i,130])*(estim[i,131]-estim[i,130])-(estim[i,131]-estim[i,130])*(orig[i,131]-orig[i,130]))/(2*(estim[i,126]-estim[i,127])*(estim[i,126]-estim[i,127])+2*(estim[i,131]-estim[i,130])*(estim[i,131]-estim[i,130]));
        BETAkl[row,1] = ((estim[i+1,126]-estim[i+1,127])*(estim[i+1,126]-estim[i+1,127])-(estim[i+1,126]-estim[i+1,127])*(orig[i+1,126]-orig[i+1,127])+(estim[i+1,131]-estim[i+1,130])*(estim[i+1,131]-estim[i+1,130])-(estim[i+1,131]-estim[i+1,130])*(orig[i+1,131]-orig[i+1,130]))/(2*(estim[i+1,126]-estim[i+1,127])*(estim[i+1,126]-estim[i+1,127])+2*(estim[i+1,131]-estim[i+1,130])*(estim[i+1,131]-estim[i+1,130]));
        BETAkl[row,2] = ((estim[i+2,126]-estim[i+2,127])*(estim[i+2,126]-estim[i+2,127])-(estim[i+2,126]-estim[i+2,127])*(orig[i+2,126]-orig[i+2,127])+(estim[i+2,131]-estim[i+2,130])*(estim[i+2,131]-estim[i+2,130])-(estim[i+2,131]-estim[i+2,130])*(orig[i+2,131]-orig[i+2,130]))/(2*(estim[i+2,126]-estim[i+2,127])*(estim[i+2,126]-estim[i+2,127])+2*(estim[i+2,131]-estim[i+2,130])*(estim[i+2,131]-estim[i+2,130])); 
        row = row+1;
        
    return BETAkl

def weighted_image2( orig, estim):
    size = int( len(orig)/3)
    
    BETAkl = np.zeros(( size , 3 ));
    
    row = 0
    length = len(orig)
    for i in range(0, length, 3):
        BETAkl[row,0]= ((estim[i,126]-estim[i,127])*(estim[i,126]-estim[i,127])-(estim[i,126]-estim[i,127])*(orig[i,126]-orig[i,127])+(estim[i,131]-estim[i,130])*(estim[i,131]-estim[i,130])-(estim[i,131]-estim[i,130])*(orig[i,131]-orig[i,130]))/(2*(estim[i,126]-estim[i,127])*(estim[i,126]-estim[i,127])+2*(estim[i,131]-estim[i,130])*(estim[i,131]-estim[i,130]));
        BETAkl[row,1] = ((estim[i+1,126]-estim[i+1,127])*(estim[i+1,126]-estim[i+1,127])-(estim[i+1,126]-estim[i+1,127])*(orig[i+1,126]-orig[i+1,127])+(estim[i+1,131]-estim[i+1,130])*(estim[i+1,131]-estim[i+1,130])-(estim[i+1,131]-estim[i+1,130])*(orig[i+1,131]-orig[i+1,130]))/(2*(estim[i+1,126]-estim[i+1,127])*(estim[i+1,126]-estim[i+1,127])+2*(estim[i+1,131]-estim[i+1,130])*(estim[i+1,131]-estim[i+1,130]));
        BETAkl[row,2] = ((estim[i+2,126]-estim[i+2,127])*(estim[i+2,126]-estim[i+2,127])-(estim[i+2,126]-estim[i+2,127])*(orig[i+2,126]-orig[i+2,127])+(estim[i+2,131]-estim[i+2,130])*(estim[i+2,131]-estim[i+2,130])-(estim[i+2,131]-estim[i+2,130])*(orig[i+2,131]-orig[i+2,130]))/(2*(estim[i+2,126]-estim[i+2,127])*(estim[i+2,126]-estim[i+2,127])+2*(estim[i+2,131]-estim[i+2,130])*(estim[i+2,131]-estim[i+2,130]));   
        row = row+1;
        
    return BETAkl