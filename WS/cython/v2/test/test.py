import timeit

#Setup using np.ndarray
setup = """
import WS as ws
path_orig = "C:/timak/WSanalysis/inputDataWS/450sekvencne0-100%/000.gx0.1-2-4.orig.txt"
path_estim = "C:/timak/WSanalysis/inputDataWS/450sekvencne0-100%/000.gx0.1-2-4.t0.txt"
	
orig = ws.path_to_matrix(path_orig)
estimated = ws.path_to_matrix(path_estim)
"""

#Setup using memarray
setup2 = """
import WS as ws
path_orig = "C:/timak/WSanalysis/inputDataWS/450sekvencne0-100%/000.gx0.1-2-4.orig.txt"
path_estim = "C:/timak/WSanalysis/inputDataWS/450sekvencne0-100%/000.gx0.1-2-4.t0.txt"
	
orig = ws.path_to_matrix2(path_orig)
estimated = ws.path_to_matrix2(path_estim)
"""

#Setup using Python code
setup3 = """
import WS_orig as ws
path_orig = "C:/timak/WSanalysis/inputDataWS/450sekvencne0-100%/000.gx0.1-2-4.orig.txt"
path_estim = "C:/timak/WSanalysis/inputDataWS/450sekvencne0-100%/000.gx0.1-2-4.t0.txt"
	
orig = ws.path_to_matrix(path_orig)
estimated = ws.path_to_matrix(path_estim)
"""

number = 1000
#setup with C division
timeCy = timeit.timeit("ws.weighted_image(orig, estimated)", setup = setup, number = number)	
print("Cython - C division:",timeCy)

#setup without C division
timeNoC = timeit.timeit("ws.weighted_image2(orig, estimated)", setup = setup, number = number)	
print("Cython - without C division:", timeNoC)

#setup without annotations
timeNoC = timeit.timeit("ws.weighted_image4(orig, estimated)", setup = setup, number = number)	
print("Cython - without annotations:", timeNoC)

#setup2 with memarrays, C division
timeMem = timeit.timeit("ws.weighted_image3(orig, estimated)", setup = setup2, number = number)	
print("Cython - C division, memArrays:", timeMem)

#setup3 with Python
timePy = timeit.timeit("ws.weighted_image(orig, estimated)", setup = setup3, number = number)	
print("Python: ", timePy)

#setup3 with Python
timePy = timeit.timeit("ws.weighted_image2(orig, estimated)", setup = setup3, number = number)	
print("Python2: ", timePy)

print("Cython is {}x faster".format( timePy / timeCy))
print("MemCython is {}x faster".format( timePy / timeMem))
	
	