# -*- coding: utf-8 -*-
#define NPY_NO_DEPRECATED_API NPY_1_7_API_VERSION

import cython
cimport cython

import numpy as np
cimport numpy as np

DTYPE = np.int
ctypedef np.int_t DTYPE_t

DTYPE_FLOAT = np.complex128
ctypedef np.complex128_t DTYPE_FLOAT_t

@cython.boundscheck(False)
@cython.wraparound(False)
@cython.cdivision(True)
@cython.nonecheck(False)
cpdef np.ndarray[DTYPE_FLOAT_t, ndim=2] weighted_image(np.ndarray[DTYPE_t, ndim=2] orig, np.ndarray[DTYPE_t, ndim=2] estim):
    cdef size_t size = len(orig)/3
    
    cdef unsigned int dim = 3
    cdef np.ndarray[DTYPE_FLOAT_t, ndim=2] BETAkl = np.zeros(( size , dim ),dtype=DTYPE_FLOAT)
    
    cdef unsigned int row = 0
    cdef size_t length = len(orig)
    cdef unsigned int i
    for i in range(0, length , 3):
        BETAkl[row,0]= ((estim[i,126]-estim[i,127])*(estim[i,126]-estim[i,127])-(estim[i,126]-estim[i,127])*(orig[i,126]-orig[i,127])+(estim[i,131]-estim[i,130])*(estim[i,131]-estim[i,130])-(estim[i,131]-estim[i,130])*(orig[i,131]-orig[i,130]))/(2*(estim[i,126]-estim[i,127])*(estim[i,126]-estim[i,127])+2*(estim[i,131]-estim[i,130])*(estim[i,131]-estim[i,130]));
        BETAkl[row,1] = ((estim[i+1,126]-estim[i+1,127])*(estim[i+1,126]-estim[i+1,127])-(estim[i+1,126]-estim[i+1,127])*(orig[i+1,126]-orig[i+1,127])+(estim[i+1,131]-estim[i+1,130])*(estim[i+1,131]-estim[i+1,130])-(estim[i+1,131]-estim[i+1,130])*(orig[i+1,131]-orig[i+1,130]))/(2*(estim[i+1,126]-estim[i+1,127])*(estim[i+1,126]-estim[i+1,127])+2*(estim[i+1,131]-estim[i+1,130])*(estim[i+1,131]-estim[i+1,130]));
        BETAkl[row,2] = ((estim[i+2,126]-estim[i+2,127])*(estim[i+2,126]-estim[i+2,127])-(estim[i+2,126]-estim[i+2,127])*(orig[i+2,126]-orig[i+2,127])+(estim[i+2,131]-estim[i+2,130])*(estim[i+2,131]-estim[i+2,130])-(estim[i+2,131]-estim[i+2,130])*(orig[i+2,131]-orig[i+2,130]))/(2*(estim[i+2,126]-estim[i+2,127])*(estim[i+2,126]-estim[i+2,127])+2*(estim[i+2,131]-estim[i+2,130])*(estim[i+2,131]-estim[i+2,130]));
        row +=1;
        
    return BETAkl

@cython.boundscheck(False)
@cython.wraparound(False)
@cython.nonecheck(False)
cpdef np.ndarray[DTYPE_FLOAT_t, ndim=2] weighted_image2(np.ndarray[DTYPE_t, ndim=2] orig, np.ndarray[DTYPE_t, ndim=2] estim):
    cdef size_t size = len(orig)/3
    
    cdef unsigned int dim = 3
    cdef np.ndarray[DTYPE_FLOAT_t, ndim=2] BETAkl = np.zeros(( size , dim ),dtype=DTYPE_FLOAT)
    
    cdef unsigned int row = 0
    cdef size_t length = len(orig)
    cdef unsigned int i
    for i in range(0, length , 3):
        BETAkl[row,0]= ((estim[i,126]-estim[i,127])*(estim[i,126]-estim[i,127])-(estim[i,126]-estim[i,127])*(orig[i,126]-orig[i,127])+(estim[i,131]-estim[i,130])*(estim[i,131]-estim[i,130])-(estim[i,131]-estim[i,130])*(orig[i,131]-orig[i,130]))/(2*(estim[i,126]-estim[i,127])*(estim[i,126]-estim[i,127])+2*(estim[i,131]-estim[i,130])*(estim[i,131]-estim[i,130]));
        BETAkl[row,1] = ((estim[i+1,126]-estim[i+1,127])*(estim[i+1,126]-estim[i+1,127])-(estim[i+1,126]-estim[i+1,127])*(orig[i+1,126]-orig[i+1,127])+(estim[i+1,131]-estim[i+1,130])*(estim[i+1,131]-estim[i+1,130])-(estim[i+1,131]-estim[i+1,130])*(orig[i+1,131]-orig[i+1,130]))/(2*(estim[i+1,126]-estim[i+1,127])*(estim[i+1,126]-estim[i+1,127])+2*(estim[i+1,131]-estim[i+1,130])*(estim[i+1,131]-estim[i+1,130]));
        BETAkl[row,2] = ((estim[i+2,126]-estim[i+2,127])*(estim[i+2,126]-estim[i+2,127])-(estim[i+2,126]-estim[i+2,127])*(orig[i+2,126]-orig[i+2,127])+(estim[i+2,131]-estim[i+2,130])*(estim[i+2,131]-estim[i+2,130])-(estim[i+2,131]-estim[i+2,130])*(orig[i+2,131]-orig[i+2,130]))/(2*(estim[i+2,126]-estim[i+2,127])*(estim[i+2,126]-estim[i+2,127])+2*(estim[i+2,131]-estim[i+2,130])*(estim[i+2,131]-estim[i+2,130]));
        row +=1;
        
    return BETAkl

cpdef np.ndarray[DTYPE_FLOAT_t, ndim=2] weighted_image4(np.ndarray[DTYPE_t, ndim=2] orig, np.ndarray[DTYPE_t, ndim=2] estim):
    cdef size_t size = len(orig)/3
    
    cdef unsigned int dim = 3
    cdef np.ndarray[DTYPE_FLOAT_t, ndim=2] BETAkl = np.zeros(( size , dim ),dtype=DTYPE_FLOAT)
    
    cdef unsigned int row = 0
    cdef size_t length = len(orig)
    cdef unsigned int i
    for i in range(0, length , 3):
        BETAkl[row,0]= ((estim[i,126]-estim[i,127])*(estim[i,126]-estim[i,127])-(estim[i,126]-estim[i,127])*(orig[i,126]-orig[i,127])+(estim[i,131]-estim[i,130])*(estim[i,131]-estim[i,130])-(estim[i,131]-estim[i,130])*(orig[i,131]-orig[i,130]))/(2*(estim[i,126]-estim[i,127])*(estim[i,126]-estim[i,127])+2*(estim[i,131]-estim[i,130])*(estim[i,131]-estim[i,130]));
        BETAkl[row,1] = ((estim[i+1,126]-estim[i+1,127])*(estim[i+1,126]-estim[i+1,127])-(estim[i+1,126]-estim[i+1,127])*(orig[i+1,126]-orig[i+1,127])+(estim[i+1,131]-estim[i+1,130])*(estim[i+1,131]-estim[i+1,130])-(estim[i+1,131]-estim[i+1,130])*(orig[i+1,131]-orig[i+1,130]))/(2*(estim[i+1,126]-estim[i+1,127])*(estim[i+1,126]-estim[i+1,127])+2*(estim[i+1,131]-estim[i+1,130])*(estim[i+1,131]-estim[i+1,130]));
        BETAkl[row,2] = ((estim[i+2,126]-estim[i+2,127])*(estim[i+2,126]-estim[i+2,127])-(estim[i+2,126]-estim[i+2,127])*(orig[i+2,126]-orig[i+2,127])+(estim[i+2,131]-estim[i+2,130])*(estim[i+2,131]-estim[i+2,130])-(estim[i+2,131]-estim[i+2,130])*(orig[i+2,131]-orig[i+2,130]))/(2*(estim[i+2,126]-estim[i+2,127])*(estim[i+2,126]-estim[i+2,127])+2*(estim[i+2,131]-estim[i+2,130])*(estim[i+2,131]-estim[i+2,130]));
        row +=1;
        
    return BETAkl

@cython.boundscheck(False)
@cython.wraparound(False)
@cython.cdivision(True)
@cython.nonecheck(False)
cpdef DTYPE_FLOAT_t[:,:] weighted_image3(DTYPE_t[:,:] orig, DTYPE_t[:,:] estim):
    cdef size_t size = len(orig)/3
    
    cdef unsigned int dim = 3
    cdef DTYPE_FLOAT_t[:,:] BETAkl = np.zeros(( size , dim ), dtype=DTYPE_FLOAT)
    
    cdef unsigned int row = 0
    cdef size_t length = len(orig)
    cdef unsigned int i
    for i in range(0, length , 3):
        BETAkl[row,0]= ((estim[i,126]-estim[i,127])*(estim[i,126]-estim[i,127])-(estim[i,126]-estim[i,127])*(orig[i,126]-orig[i,127])+(estim[i,131]-estim[i,130])*(estim[i,131]-estim[i,130])-(estim[i,131]-estim[i,130])*(orig[i,131]-orig[i,130]))/(2*(estim[i,126]-estim[i,127])*(estim[i,126]-estim[i,127])+2*(estim[i,131]-estim[i,130])*(estim[i,131]-estim[i,130]));
        BETAkl[row,1] = ((estim[i+1,126]-estim[i+1,127])*(estim[i+1,126]-estim[i+1,127])-(estim[i+1,126]-estim[i+1,127])*(orig[i+1,126]-orig[i+1,127])+(estim[i+1,131]-estim[i+1,130])*(estim[i+1,131]-estim[i+1,130])-(estim[i+1,131]-estim[i+1,130])*(orig[i+1,131]-orig[i+1,130]))/(2*(estim[i+1,126]-estim[i+1,127])*(estim[i+1,126]-estim[i+1,127])+2*(estim[i+1,131]-estim[i+1,130])*(estim[i+1,131]-estim[i+1,130]));
        BETAkl[row,2] = ((estim[i+2,126]-estim[i+2,127])*(estim[i+2,126]-estim[i+2,127])-(estim[i+2,126]-estim[i+2,127])*(orig[i+2,126]-orig[i+2,127])+(estim[i+2,131]-estim[i+2,130])*(estim[i+2,131]-estim[i+2,130])-(estim[i+2,131]-estim[i+2,130])*(orig[i+2,131]-orig[i+2,130]))/(2*(estim[i+2,126]-estim[i+2,127])*(estim[i+2,126]-estim[i+2,127])+2*(estim[i+2,131]-estim[i+2,130])*(estim[i+2,131]-estim[i+2,130]));
        row +=1;
        
    return BETAkl

cpdef np.ndarray[DTYPE_t, ndim=2] path_to_matrix(str path):
    cdef np.ndarray[DTYPE_t, ndim=2] matrix = np.loadtxt(path, dtype=DTYPE)
    
    return matrix

cpdef DTYPE_t[:,:] path_to_matrix2(str path):
    cdef DTYPE_t[:,:] matrix = np.loadtxt( path, dtype=DTYPE)
    
    return matrix