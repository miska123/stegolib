#include <stdio.h>
#include <iostream>
#include "utils.cpp"

using namespace std;

std::vector< std::vector<double> > vyber1frekvenciu(std::vector< std::vector<double> > DCT, int index_row, int index_col) {
	int dctLen = DCT.size(); //len(dct)
	int dctElements = int(DCT[0].size() * dctLen); //dct.size
	int num_rows = int( dctLen / 8);
	int num_cols = int((dctElements / dctLen) / 8);
	cout << num_rows << endl;
	cout << num_cols << endl;

	std::vector< std::vector<double> > G = zeros(num_rows, num_cols);

	int row_G = 0;
	for (int y = index_row; y < num_rows; y += 8) {
		//pre kazdy nulty riadok - vynechavame AC koeficienty
		int jump = index_col;
		for (int x = 0; x < num_cols; x += 1) {
			G[row_G][x] = DCT[y][jump];
			jump += 8;
		}
		row_G += 1;
	}
	return G;

}

double myVariation(std::vector<double> vector) {
	double variation = 0;
	int maxIter = vector.size() - 1;
	for(int i = 0; i < maxIter; i++) {
		variation += abs(vector[i + 1] - vector[i]);
	}
	return variation;

}

std::vector< std::vector<double> > odrez(std::vector< std::vector<double> > G) {
	int n = G.size() % 4; // upravi na velkost delit. 5
	//printf("orezavam: %d %s", n, "prvkov");
	std::vector< std::vector<double> >::const_iterator first = G.begin();
	std::vector< std::vector<double> >::const_iterator last = G.begin() + (G.size()-n);
	//vector<T> newVec(first, last);
	//TODO toto tiez doriesit
	//G = G(&G[0], &G[100]);
	return G;
}

int velkostSkupin(int size){
	if (size % 5 == 0) {
		return 5;
	}
	else if (size % 4 == 0) {
		return 4;
	}
	else if (size % 3 == 0) {
		return 3;
	}
	else if (size % 6 == 0) {
		return 6;
	}
	else {
		//printf("Nie je delitelna 6, 5, 4, ani 3!!!");
		return 0;
	}
}

std::vector<double> discriminacnaF(std::vector< std::vector<double> > G) {
	int len = G.size();
	std::vector<double> f_G = zeros1d( G.size());

	for (int i = 0; i < len; i++) {
		f_G[i] = myVariation(G[i]);
	}

	return f_G;
}

std::vector< std::vector<double> > loadtxt2(istream &file) {
	std::string str;
	std::vector <std::vector<double> > arr2D;
	while (std::getline(file, str))
	{
		//std::getline(file, str);
		//std::cout << str << std::endl;
		std::stringstream iss(str);
		double number;
		std::vector<double> myNumbers;
		while (iss >> number)
			myNumbers.push_back(number);
		if (myNumbers.size() > 0) { //odignorujeme prazdne riadky
			arr2D.push_back(myNumbers);
		}

	}
	//std::cout << arr2D.size() << std::endl;
	//std::cout << arr2D[0].size() << std::endl;

	return arr2D;
}

std::vector <std::vector<double> > reshape(vector < vector<double> > vec, int n, int num_rows) {
	std::vector< std::vector<double> >::const_iterator row;
	std::vector<double>::const_iterator col;

	std::vector <std::vector<double> > arr2D;
	std::vector<double> myNumbers;

	int index = 0;
	for (row = vec.begin(); row != vec.end(); ++row)
	{
		for (col = row->begin(); col != row->end(); ++col)
		{
			myNumbers.push_back(*col);
			index++;
			if (index == n) {
				arr2D.push_back(myNumbers);
				myNumbers.clear();
				index = 0;
			}
			//std::cout << *col;
		}
		//std::cout << std::endl;
	}

	std::cout << arr2D.size() << std::endl;
	std::cout << arr2D[0].size() << std::endl;
	std::cout << arr2D[2][2] << std::endl;

	return arr2D;
}

std::vector< std::vector<double> > createMask(std::vector< std::vector<double> > G, int value) {
	int rowLen = G[0].size();
	std::vector< std::vector<double> > M = zeros(1, rowLen);

	for (int m = rowLen - (rowLen - 1); m < (rowLen - 1); m++) {
		M[0][m] = value;
	}
	return M;
}

int flipping1(int number) {
	//permutacie
	if (number % 2 == 0) {
		// ak je parne
		number += 1;
		return number;
	}
	else {
		number -= 1;
		return number;
	}
}

int flipping2(int number) { 
	// inverzny flipping operation
	if (number % 2 != 0) {
		// ak nie je parne
		number += 1;
		return number;
	}
	else {
		number -= 1;
		return number;
	}
}

std::vector< std::vector<double> > applyMask(std::vector< std::vector<double> > maskedG, std::vector< std::vector<double> > M) {
	int len = maskedG.size();
	int rowLen = maskedG[0].size();

	for (int rows = 0; rows < len; rows++) {
		for (int cols = 0; cols < rowLen; cols++) {
			if (M[0][cols] == 1) {
				maskedG[rows][cols] = flipping1(maskedG[rows][cols]);
			}
			else if (M[0][cols] == -1) {
				maskedG[rows][cols] = flipping2(maskedG[rows][cols]);
			}
			else {
				maskedG[rows][cols] = maskedG[rows][cols];
			}
		}
	}
	return maskedG;
}

void rstest() {
	std::vector<double> regular(std::vector<double>(101, 0));
	std::vector<double> singular(std::vector<double>(101, 0));
	std::vector<double> regular_(std::vector<double>(101, 0));
	std::vector<double> singular_(std::vector<double>(101, 0));
	std::vector<double> rozdielR(std::vector<double>(101, 0));
	std::vector<double> rozdielS(std::vector<double>(101, 0));
	std::vector<double> rozdielRS(std::vector<double>(101, 0));

	for (int y = 0; y < 101; y++) {
		string num = padZeros(y, 3);
		ostringstream dctPath;
		dctPath << "C:\\timak\\RSanalysis\\inputDataRS\\inputRS\\koefLSB0-100\\stego_out_lsb" << num << ".txt";
		string dctPathStr = dctPath.str();
		cout << dctPathStr << std::endl;
		ifstream dctfile(dctPathStr);
		vector < vector<double> > DCT = loadtxt2(dctfile);
		//printf("DCT len %d\n", DCT.size());
		if (DCT.size() > 0) {
			cout << DCT[0][8] << std::endl;
		}

		int index_row = 0;
		int index_col = 1;
		cout << "G[0] pred:" << DCT[0][817] << endl;
		vector < vector<double>> G = vyber1frekvenciu(DCT, index_row, index_col);
		cout << "G[0] pred:" << G[1][102] << endl;

		int n = velkostSkupin(G.size() * G[0].size());
		if (n == 0) {
			G = odrez(G);
			n = velkostSkupin(G.size() * G[0].size());
		}
		//printf("velkost skupiniek je: %d\n", n);
		G = reshape(G, n, G.size());

		vector<double > f_G = discriminacnaF(G);
		cout << "fg:" << f_G.size() << endl;
		std::vector< std::vector<double> > M = createMask(G, 1);
		cout << "Copying" << endl;
		std::vector< std::vector<double> > maskedG = G;
		maskedG = applyMask(maskedG, M);
		std::vector<double> f_maskedG = discriminacnaF(maskedG);
		cout << "fmasked:" << f_maskedG.size() << endl;

		int Regular = 0;
		int Singular = 0;
		int Unusable = 0;

		int lengthFG = f_G.size();
		for (int i = 0; i < lengthFG; i++) {
			if (f_maskedG[i] > f_G[i]) {
				Regular += 1;
			}
			else if (f_maskedG[i] < f_G[i]) {
				Singular += 1;
			}
			else {
				Unusable += 1;
			}
		}

		double lenFG = (double)lengthFG;
		cout << "relat. pocet Regular skupin = " << Regular << " " << Regular / lenFG * 100 << "%" << endl;
		cout << "relat. pocet Singular skupin = " << Singular << " " << Singular / lenFG * 100 << "%" << endl;
		cout << "relat. pocet Unusable skupin = " << Unusable << " " << Unusable / lenFG * 100 << "%" << endl;

		//8. aplikujeme "inverznu masku M" **********************
		// print("G before inverze", G[0])
		std::vector< std::vector<double> > inv_M = createMask(G, -1);
		//  print("inverzna maska M = ", inv_M)
		std::vector< std::vector<double> > inv_maskedG = G; //# odovzdam referenciu na objekt
		inv_maskedG = applyMask(inv_maskedG, inv_M);
		// print("inv_maskedG[0]:", inv_maskedG[0])

		//9. zopakujeme len s inverznym polovom
		std::vector<double> f_inv_maskedG = discriminacnaF(inv_maskedG);

		int Regular_ = 0;
		int Singular_ = 0;
		int Unusable_ = 0;

		for (int i = 0; i < lengthFG; i++) {
			if (f_inv_maskedG[i] > f_G[i]) {
				Regular_ += 1;
			}
			else if (f_inv_maskedG[i] < f_G[i]) {
				Singular_ += 1;
			}
			else {
				Unusable_ += 1;
			}
		}

		cout << "relat. pocet Regular inverz. skupin = " << Regular_ / lenFG * 100 << "%" << endl;
		cout << "relat. pocet Singular inverz. skupin = " << Singular_ / lenFG * 100 << "%" << endl;
		cout << "relat. pocet Unusable inverz. skupin = " << Unusable_ / lenFG * 100 << "%" << endl;

		regular[y] = Regular / lenFG * 100;
		singular[y] = Singular / lenFG * 100;
		regular_[y] = Regular_ / lenFG * 100;
		singular_[y] = Singular_ / lenFG * 100;

		rozdielR[y] = abs((Regular / lenFG * 100) - (Regular_ / lenFG * 100));
		rozdielS[y] = abs((Singular / lenFG * 100) - (Singular_ / lenFG * 100));
		rozdielRS[y] = abs((Regular / lenFG * 100) - (Singular / lenFG * 100));
		/*
		cout << "reg= " << regular[y] << endl;
		cout << "reg_= " << regular_[y] << endl;
		cout << "sing= " << singular[y] << endl;
		cout << "sing_= " << singular_[y] << endl;
		cout << "rozr= " << rozdielR[y] << endl;
		cout << "rozs= " << rozdielS[y] << endl;
		cout << "rozrs= " << rozdielRS[y] << endl;*/
	}
}

int main(int argc, const char* argv[])
{
	rstest();
	system("pause");
	return 0;
}

