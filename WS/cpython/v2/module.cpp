#include <Python.h>
#include <numpy/arrayobject.h>
#include <stdio.h>
#include <iostream>
#include <vector>
#include <fstream>
#include <sstream>
#include <string>
#include <iomanip>
#include "rs_test.cpp"
#include "ws_test.cpp"

using namespace std;

void printVector(const std::vector< std::vector<int> > & vec)
{

    std::vector< std::vector<int> >::const_iterator row;
    std::vector<int>::const_iterator col;

    for (row = vec.begin(); row != vec.end(); ++row)
    {
         for (col = row->begin(); col != row->end(); ++col)
         {
            std::cout << *col;
         }
		 std::cout << std::endl;
    }

}

PyObject* load(PyObject *self, PyObject *arg) {
	
	char *path;

	if (!PyArg_Parse(arg, "s", &path))
		return NULL;
	
	PyObject* matrix = getMatrix( path );
	/*
	Py_INCREF(Py_None);
	ret = Py_None;
	assert(!PyErr_Occurred());
	assert(ret);
	goto finally;
	except:
		Py_XDECREF(ret);
		ret = NULL;
	finally:
		/* Treat arg as a borrowed reference. 
		Py_DECREF(arg);
		*/
	return matrix;
}

PyObject* wptest(PyObject *self, PyObject *args) {
	PyArrayObject *orig;
	PyArrayObject *estim;

	if (!PyArg_ParseTuple(args, "OO", &orig, &estim))
		return NULL;

	if (NULL == orig)  return NULL;
	if (NULL == estim)  return NULL;
	
	PyObject* result = getWeightedImage(orig, estim);
	
	return result;
}

PyObject* rstest(PyObject *self, PyObject *args) {
	rstest();

	return PyFloat_FromDouble(0);
}

static PyMethodDef stego_methods[] = {
	// The first property is the name exposed to python,
	// the second is the C++ function name        
	{ "ws_path_to_matrix", (PyCFunction)load, METH_O, "Load data from file" },
	{ "ws_weighted_image", (PyCFunction)wptest, METH_VARARGS , "Calculate BETAkl" },
	{ "rs", (PyCFunction)rstest, METH_NOARGS, nullptr },

	// Terminate the array with an object containing nulls.
	{ nullptr, nullptr, 0, nullptr }
};


static PyModuleDef stegolib_module = {
	PyModuleDef_HEAD_INIT,
	"Stegolib",                        // Module name as Python sees it
	"Stego analalysis library",  // Module description
	0,
	stego_methods						  // Structure that defines the methods
};

PyMODINIT_FUNC PyInit_stegolib() {
	import_array();
	return PyModule_Create(&stegolib_module);
}