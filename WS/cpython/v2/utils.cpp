#pragma once
#include <vector>
#include <iostream>
#include <vector>
#include <fstream>
#include <sstream>
#include <string>
#include <iomanip>

int inc(int i) {
	i++;
	return i;
}

void printVector(const std::vector< std::vector<double> > & vec)
{

	std::vector< std::vector<double> >::const_iterator row;
	std::vector<double>::const_iterator col;

	for (row = vec.begin(); row != vec.end(); ++row)
	{
		for (col = row->begin(); col != row->end(); ++col)
		{
			std::cout << *col;
		}
		std::cout << std::endl;
	}

}
/*
std::vector< std::vector<int> > loadtxt(istream &file) {
	std::string str;
	std::vector <std::vector<int> > arr2D;
	while (std::getline(file, str))
	{
		//std::getline(file, str);
		//std::cout << str << std::endl;
		std::stringstream iss(str);
		int number;
		std::vector<int> myNumbers;
		while (iss >> number)
			myNumbers.push_back(number);
		if (myNumbers.size() > 0) { //odignorujeme prazdne riadky
			arr2D.push_back(myNumbers);
		}

	}
	//std::cout << arr2D.size() << std::endl;
	//std::cout << arr2D[0].size() << std::endl;

	return arr2D;
}
*/
std::string padZeros(int number, int width) {

	std::ostringstream ss;
	ss << std::setw(width) << std::setfill('0') << number;
	std::string str = ss.str();
	//std::cout << str << std::endl;
	return str;
}

// Vytvori 2D Vector array naplnene nulami. Rozmery arrayu su dane parametrami funkcie.
std::vector< std::vector<double> > zeros(int height, int width) {

	std::vector< std::vector<double> > nullArray;
	for (int i = 0; i < height; i++) {
		std::vector<double> row(width, 0.0);
		nullArray.push_back(row);
	}
	return nullArray;
}

std::vector< std::vector<float> > zerosFloat(int height, int width) {

	std::vector< std::vector<float> > nullArray;
	for (int i = 0; i < height; i++) {
		std::vector<float> row(width, 0.0);
		nullArray.push_back(row);
	}
	return nullArray;
}

// Vytvori 1D Vector array naplnene nulami. Rozmery arrayu su dane parametrami funkcie.
std::vector<double> zeros1d(int width) {

	std::vector<double> row(width, 0.0);
	return row;
}