import timeit

#Setup using np.ndarray
setup = """
import stegolib as ws

path_orig = "C:/timak/WSanalysis/inputDataWS/450sekvencne0-100%/000.gx0.1-2-4.orig.txt"
path_estim = "C:/timak/WSanalysis/inputDataWS/450sekvencne0-100%/000.gx0.1-2-4.t0.txt"
orig = ws.ws_path_to_matrix( path_orig )
estim = ws.ws_path_to_matrix( path_estim )
"""
	
#BETAkl = ws.ws_weighted_image( orig, estim )
number = 1000
#setup with C division
timeCPy = timeit.timeit("ws.ws_weighted_image( orig, estim )", setup = setup, number = number)	
print("CPython:",timeCPy)

'''
import stegolib as ws

path_orig = "C:/timak/WSanalysis/inputDataWS/450sekvencne0-100%/000.gx0.1-2-4.orig.txt"
path_estim = "C:/timak/WSanalysis/inputDataWS/450sekvencne0-100%/000.gx0.1-2-4.t0.txt"
orig = ws.ws_path_to_matrix( path_orig )
estim = ws.ws_path_to_matrix( path_estim )
BETA = ws.ws_weighted_image( orig, estim )

print(BETA[0])
'''