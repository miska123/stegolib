#include <Python.h>
#include <numpy/arrayobject.h>
#include <stdio.h>
#include <iostream>
#include "utils.cpp"

using namespace std;

std::vector< std::vector<int> > loadtxt(istream &file) {
	std::string str;
	std::vector <std::vector<int> > arr2D;
	while (std::getline(file, str))
	{
		//std::getline(file, str);
		//std::cout << str << std::endl;
		std::stringstream iss(str);
		int number;
		std::vector<int> myNumbers;
		while (iss >> number)
			myNumbers.push_back(number);
		if (myNumbers.size() > 0) { //odignorujeme prazdne riadky
			arr2D.push_back(myNumbers);
		}

	}

	return arr2D;
}

vector < vector<double> > weighted_image(vector < vector<int> > orig, vector < vector<int> > estim) {
	int origSize = orig.size();
	vector< vector<double> > BETAkl = zeros(origSize / 3, 3);
	vector< vector<double> > BETA = zeros(origSize / 3, 1);

	int row = 0;
	for (int i = 0; i < origSize; i += 3) {

		BETAkl[row][0] = (((double)estim[i][126] - (double)estim[i][127])*((double)estim[i][126] - (double)estim[i][127]) - ((double)estim[i][126] - (double)estim[i][127])*((double)orig[i][126] - (double)orig[i][127]) + ((double)estim[i][131] - (double)estim[i][130])*((double)estim[i][131] - (double)estim[i][130]) - ((double)estim[i][131] - (double)estim[i][130])*((double)orig[i][131] - (double)orig[i][130])) / (2.0*((double)estim[i][126] - (double)estim[i][127])*((double)estim[i][126] - (double)estim[i][127]) + 2.0*((double)estim[i][131] - (double)estim[i][130])*((double)estim[i][131] - (double)estim[i][130]));
		BETAkl[row][1] = (((double)estim[i + 1][126] - (double)estim[i + 1][127])*((double)estim[i + 1][126] - (double)estim[i + 1][127]) - ((double)estim[i + 1][126] - (double)estim[i + 1][127])*((double)orig[i + 1][126] - (double)orig[i + 1][127]) + ((double)estim[i + 1][131] - (double)estim[i + 1][130])*((double)estim[i + 1][131] - (double)estim[i + 1][130]) - ((double)estim[i + 1][131] - (double)estim[i + 1][130])*((double)orig[i + 1][131] - (double)orig[i + 1][130])) / (2 * ((double)estim[i + 1][126] - (double)estim[i + 1][127])*((double)estim[i + 1][126] - (double)estim[i + 1][127]) + 2 * ((double)estim[i + 1][131] - (double)estim[i + 1][130])*((double)estim[i + 1][131] - (double)estim[i + 1][130]));
		BETAkl[row][2] = (((double)estim[i + 2][126] - (double)estim[i + 2][127])*((double)estim[i + 2][126] - (double)estim[i + 2][127]) - ((double)estim[i + 2][126] - (double)estim[i + 2][127])*((double)orig[i + 2][126] - (double)orig[i + 2][127]) + ((double)estim[i + 2][131] - (double)estim[i + 2][130])*((double)estim[i + 2][131] - (double)estim[i + 2][130]) - ((double)estim[i + 2][131] - (double)estim[i + 2][130])*((double)orig[i + 2][131] - (double)orig[i + 2][130])) / (2 * ((double)estim[i + 2][126] - (double)estim[i + 2][127])*((double)estim[i + 2][126] - (double)estim[i + 2][127]) + 2 * ((double)estim[i + 2][131] - (double)estim[i + 2][130])*((double)estim[i + 2][131] - (double)estim[i + 2][130]));
		BETA[row][0] = (BETAkl[row][0] + BETAkl[row][1] + BETAkl[row][2]) / 3;

		row++;
	}
	return BETAkl;
}

template<typename T>
PyObject* vectorToMatrix(const vector<vector<T>> &data, int type_num= PyArray_INT) {
	// rows not empty
	/*if (!data.empty()) {

		// column not empty
		if (!data[0].empty()) {*/
			PyArrayObject *matout;
			size_t nRows = data.size();
			size_t nCols = data[0].size();
			int dims[2] = { nRows, nCols };
			
			matout = (PyArrayObject*) PyArray_FromDims(2, dims, type_num);
			if (!matout) throw logic_error("Unable to allocate memory for Python array");
			
			T *pointer = (T*)PyArray_DATA(matout);

			// copy vector line by line
			for (size_t i = 0; i < nRows; ++i) {

				if (data[i].size() != nCols) {
					throw(string("Can not convert vector<vector<T>> to np.array."));
				}

				copy(data[i].begin(), data[i].end(), pointer + i * nCols);
			}

			return PyArray_Return(matout);
/*
		}

	}*/
}


vector< vector<int> > path_to_matrix(char* path)
{
	ostringstream str;
	str.str(path);
	std::cout << "PATH IS " << str.str() << '\n';
	//string inputfilePath1 = path;
	ifstream inputfile1(path);
	vector < vector<int> > matrix = loadtxt(inputfile1);

	return matrix;
}


PyObject* getMatrix(char* path) {
	//import_array(-1);
	vector< vector<int> > data = path_to_matrix(path);
	PyObject* rows = vectorToMatrix(data);
	return rows;
}

PyObject* getWeightedImage(const vector<vector<int>> &orig, const vector<vector<int>> &estim) {
	vector < vector<double> > data = weighted_image(orig, estim);
}
