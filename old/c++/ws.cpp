#pragma once
#include <stdio.h>
#include <iostream>
#include "utils.cpp"

using namespace std;

std::vector< std::vector<int> > loadtxt(istream &file) {
	std::string str;
	std::vector <std::vector<int> > arr2D;
	while (std::getline(file, str))
	{
		//std::getline(file, str);
		//std::cout << str << std::endl;
		std::stringstream iss(str);
		int number;
		std::vector<int> myNumbers;
		while (iss >> number)
			myNumbers.push_back(number);
		if (myNumbers.size() > 0) { //odignorujeme prazdne riadky
			arr2D.push_back(myNumbers);
		}

	}

	return arr2D;
}

vector < vector<double> > weighted_image(vector < vector<int> > orig, vector < vector<int> > estim) {
	int origSize = orig.size();
	vector< vector<double> > BETAkl = zeros(origSize / 3, 3);
	vector< vector<double> > BETA = zeros(origSize / 3, 1);

	int row = 0;
	for (int i = 0; i < origSize; i += 3) {

		BETAkl[row][0] = (((double)estim[i][126] - (double)estim[i][127])*((double)estim[i][126] - (double)estim[i][127]) - ((double)estim[i][126] - (double)estim[i][127])*((double)orig[i][126] - (double)orig[i][127]) + ((double)estim[i][131] - (double)estim[i][130])*((double)estim[i][131] - (double)estim[i][130]) - ((double)estim[i][131] - (double)estim[i][130])*((double)orig[i][131] - (double)orig[i][130])) / (2.0*((double)estim[i][126] - (double)estim[i][127])*((double)estim[i][126] - (double)estim[i][127]) + 2.0*((double)estim[i][131] - (double)estim[i][130])*((double)estim[i][131] - (double)estim[i][130]));
		BETAkl[row][1] = (((double)estim[i + 1][126] - (double)estim[i + 1][127])*((double)estim[i + 1][126] - (double)estim[i + 1][127]) - ((double)estim[i + 1][126] - (double)estim[i + 1][127])*((double)orig[i + 1][126] - (double)orig[i + 1][127]) + ((double)estim[i + 1][131] - (double)estim[i + 1][130])*((double)estim[i + 1][131] - (double)estim[i + 1][130]) - ((double)estim[i + 1][131] - (double)estim[i + 1][130])*((double)orig[i + 1][131] - (double)orig[i + 1][130])) / (2 * ((double)estim[i + 1][126] - (double)estim[i + 1][127])*((double)estim[i + 1][126] - (double)estim[i + 1][127]) + 2 * ((double)estim[i + 1][131] - (double)estim[i + 1][130])*((double)estim[i + 1][131] - (double)estim[i + 1][130]));
		BETAkl[row][2] = (((double)estim[i + 2][126] - (double)estim[i + 2][127])*((double)estim[i + 2][126] - (double)estim[i + 2][127]) - ((double)estim[i + 2][126] - (double)estim[i + 2][127])*((double)orig[i + 2][126] - (double)orig[i + 2][127]) + ((double)estim[i + 2][131] - (double)estim[i + 2][130])*((double)estim[i + 2][131] - (double)estim[i + 2][130]) - ((double)estim[i + 2][131] - (double)estim[i + 2][130])*((double)orig[i + 2][131] - (double)orig[i + 2][130])) / (2 * ((double)estim[i + 2][126] - (double)estim[i + 2][127])*((double)estim[i + 2][126] - (double)estim[i + 2][127]) + 2 * ((double)estim[i + 2][131] - (double)estim[i + 2][130])*((double)estim[i + 2][131] - (double)estim[i + 2][130]));
		BETA[row][0] = (BETAkl[row][0] + BETAkl[row][1] + BETAkl[row][2]) / 3;

		row++;
	}
	return BETA;
}

vector< vector<double> > wptest(ostringstream ssInputfilePath1, ostringstream ssInputfilePath2)
{
	std::vector<int> avgBETA(std::vector<int>(101, 0)); //np.zeros

	for (int y = 0; y < 101; y++) {
		string num = padZeros(y, 3);
		/*
		ostringstream ssInputfilePath1;
		ssInputfilePath1 << "C:\timak\RSanalysis\inputDataRS\inputRS\koefLSB0-100" << num << ".gx0.1-2-4.orig.txt";

		ostringstream ssInputfilePath2;
		ssInputfilePath2 << "C:\timak\RSanalysis\inputDataRS\inputRS\koefLSB0-100" << num << ".gx0.1-2-4.t0.txt";
		*/
		string inputfilePath1 = ssInputfilePath1.str();
		string inputfilePath2 = ssInputfilePath2.str();
		/*
		ostringstream ssOutputfilePath;
		ssOutputfilePath << "C:\timak\RSanalysis\\" << num << "orig-t0.txt";
		string outputfilePath = ssOutputfilePath.str();
		*/
		ifstream inputfile1(inputfilePath1);
		ifstream inputfile2(inputfilePath2);
		vector < vector<int> > orig = loadtxt(inputfile1);
		vector < vector<int> > estim = loadtxt(inputfile2);

		vector < vector<double> > BETA = weighted_image(orig, estim);
	}
}