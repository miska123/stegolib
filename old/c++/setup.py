from distutils.core import setup, Extension#, DEBUG

sfc_module = Extension('TeamProject', sources = ['module.cpp'])

setup(name = 'TeamProject', version = '1.3',
    description = 'Python Package with TeamProject C++ Extension',
    ext_modules = [sfc_module]
    )